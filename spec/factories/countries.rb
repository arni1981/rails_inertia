FactoryBot.define do
  factory :country do
    name { "MyString" }
    sensitive_date { 1 }
  end
end
