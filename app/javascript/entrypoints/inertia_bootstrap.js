import { createInertiaApp } from '@inertiajs/svelte'
import Layout from '../components/Layout.svelte'

createInertiaApp({
    resolve: name => {
        const pages = import.meta.glob('../pages/**/*.svelte', { eager: true })
        const page = pages[`../pages/${name}.svelte`]
        console.log(page);

        return { default: page.default, layout: page.layout || Layout }
    },
    setup({ el, App, props }) {
        new App({ target: el, props })
    },
})
