class CountriesController < ApplicationController
  def index
    render inertia: 'Index', props: {
      countries: CountryBlueprint.render_as_hash(Country.all)
    }
  end

  def show
    render inertia: 'Show', props: {
      country: CountryBlueprint.render_as_hash(Country.find(params[:id]))
    }
  end
end
