# Ruby on Rails with IntertiaJS and Svelte

This includes a basic setup for InteriaJs with Svelte, using vite behind the scenes.

```
rails db:create
rails db:migrate
rails db:seed

rails s
# and go to localhost:3000/countries
```
